#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
import locale

from gi.repository import Gtk, Gio, GObject, Adw

from mcg.zeroconf import ZeroconfProvider




@Gtk.Template(resource_path='/xyz/suruatoel/mcg/ui/connection-panel.ui')
class ConnectionPanel(Adw.Bin):
    __gtype_name__ = 'McgConnectionPanel'
    __gsignals__ = {
        'connection-changed': (GObject.SIGNAL_RUN_FIRST, None, (str, int, str))
    }

    # Widgets
    toolbar = Gtk.Template.Child()
    zeroconf_list = Gtk.Template.Child()
    host_row = Gtk.Template.Child()
    port_spinner = Gtk.Template.Child()
    password_row = Gtk.Template.Child()


    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Zeroconf provider
        self._zeroconf_provider = ZeroconfProvider()
        self._zeroconf_provider.connect_signal(ZeroconfProvider.SIGNAL_SERVICE_NEW, self.on_new_service)


    def on_new_service(self, service):
        name, host, port = service

        row_button = Gtk.Button()
        row_button.set_label(locale.gettext("use"))
        row_button.connect("clicked", self.on_service_selected, host, port)

        row = Adw.ActionRow()
        row.set_title(name)
        row.set_subtitle("{} ({})".format(host, port))
        row.add_suffix(row_button)

        self.zeroconf_list.insert(row, -1)


    def on_service_selected(self, widget, host, port):
        self.set_host(host)
        self.set_port(port)


    @Gtk.Template.Callback()
    def on_host_entry_apply(self, widget):
        self._call_back()


    @Gtk.Template.Callback()
    def on_port_spinner_value_changed(self, widget):
        self._call_back()


    def set_host(self, host):
        self.host_row.set_text(host)


    def get_host(self):
        return self.host_row.get_text()


    def set_port(self, port):
        self.port_spinner.set_value(port)


    def get_port(self):
        return self.port_spinner.get_value_as_int()


    def set_password(self, password):
        if password is None:
            password = ""
        self.password_row.set_text(password)


    def get_password(self):
        if self.password_row.get_text() == "":
            return None
        else:
            return self.password_entry.get_text()


    def _call_back(self):
        self.emit('connection-changed', self.get_host(), self.get_port(), self.get_password(),)
