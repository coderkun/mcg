#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk




@Gtk.Template(resource_path='/xyz/suruatoel/mcg/ui/shortcuts-dialog.ui')
class ShortcutsDialog(Gtk.ShortcutsWindow):
    __gtype_name__ = 'McgShortcutsDialog'


    def __init__(self):
        super().__init__()
