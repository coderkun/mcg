import sys

from .application import Application



def main(version):
    app = Application()
    return app.run(sys.argv)
