#!/usr/bin/env python3


import logging
import urllib

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gio, Gtk, Gdk, GLib, Adw

from .window import Window




class Application(Gtk.Application):
    TITLE = "CoverGrid"
    ID = 'xyz.suruatoel.mcg'
    DOMAIN = 'mcg'


    def __init__(self):
        super().__init__(application_id=Application.ID, flags=Gio.ApplicationFlags.FLAGS_NONE)
        self._window = None
        self._info_dialog = None
        self._verbosity = logging.WARNING
        self.set_accels_for_action('window.close', ['<primary>q'])
        self.set_accels_for_action('win.show-help-overlay', ['<primary>k'])
        self.set_accels_for_action('app.info', ['<primary>i'])
        self.set_accels_for_action('win.connect', ['<primary>c'])
        self.set_accels_for_action('win.play', ['<primary>p'])
        self.set_accels_for_action('win.clear-playlist', ['<primary>r'])
        self.set_accels_for_action('win.toggle-fullscreen', ['F11'])
        self.set_accels_for_action('win.search-library', ['<primary>f'])
        self.set_accels_for_action('win.panel("0")', ['<primary>KP_1'])
        self.set_accels_for_action('win.panel("1")', ['<primary>KP_2'])
        self.set_accels_for_action('win.panel("2")', ['<primary>KP_3'])
        self.set_accels_for_action('win.panel("3")', ['<primary>KP_4'])


    def do_startup(self):
        Gtk.Application.do_startup(self)
        self._setup_logging()
        self._load_settings()
        self._set_default_settings()
        self._load_css()
        self._setup_actions()
        self._setup_adw()


    def do_activate(self):
        Gtk.Application.do_activate(self)
        if not self._window:
            self._window = Window(self, Application.TITLE, self._settings)
        self._window.present()


    def on_menu_info(self, action, value):
        self._info_dialog = Adw.AboutDialog()
        self._info_dialog.set_application_icon("xyz.suruatoel.mcg")
        self._info_dialog.set_application_name("CoverGrid")
        self._info_dialog.set_version("3.2.1")
        self._info_dialog.set_comments("CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks.")
        self._info_dialog.set_website("https://www.suruatoel.xyz/codes/mcg")
        self._info_dialog.set_license_type(Gtk.License.GPL_3_0)
        self._info_dialog.set_issue_url("https://git.suruatoel.xyz/coderkun/mcg")
        self._info_dialog.present()


    def on_menu_quit(self, action, value):
        self.quit()


    def _setup_logging(self):
        logging.basicConfig(
            level=self._verbosity,
            format="%(asctime)s %(levelname)s: %(message)s"
        )


    def _load_settings(self):
        self._settings = Gio.Settings.new(Application.ID)


    def _set_default_settings(self):
        style_manager = Adw.StyleManager.get_default()
        style_manager.set_color_scheme(Adw.ColorScheme.PREFER_DARK)


    def _load_css(self):
        styleProvider = Gtk.CssProvider()
        styleProvider.load_from_resource(self._get_resource_path('gtk.css'))
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            styleProvider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )


    def _setup_actions(self):
        action = Gio.SimpleAction.new("info", None)
        action.connect('activate', self.on_menu_info)
        self.add_action(action)
        action = Gio.SimpleAction.new("quit", None)
        action.connect('activate', self.on_menu_quit)
        self.add_action(action)


    def _get_resource_path(self, path):
        return "/{}/{}".format(Application.ID.replace('.', '/'), path)


    def _setup_adw(self):
        Adw.HeaderBar()
        Adw.ToolbarView()
        Adw.ViewSwitcherTitle()
        Adw.ViewSwitcherBar()
        Adw.ViewStackPage()
        Adw.ToastOverlay()
        Adw.StatusPage()
        Adw.Flap()
        Adw.EntryRow()
        Adw.PasswordEntryRow()
