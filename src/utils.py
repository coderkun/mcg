#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '4.0')
import hashlib
import locale
import os
import urllib

from gi.repository import Gdk, GdkPixbuf, GObject, Gtk




class Utils:
    CSS_SELECTION = 'selection'
    STOCK_ICON_DEFAULT = 'image-x-generic-symbolic'


    def load_pixbuf(data):
        loader = GdkPixbuf.PixbufLoader()
        try:
            loader.write(data)
        finally:
            loader.close()
        return loader.get_pixbuf()


    def load_thumbnail(cache, client, album, size):
        cache_url = cache.create_filename(album)
        pixbuf = None

        if os.path.isfile(cache_url):
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(cache_url)
        else:
            # Load cover from server
            albumart = client.get_albumart_now(album.get_id())
            if albumart:
                pixbuf = Utils.load_pixbuf(albumart)
            if pixbuf is not None:
                pixbuf = pixbuf.scale_simple(size, size, GdkPixbuf.InterpType.HYPER)
                pixbuf.savev(cache_url, 'jpeg', [], [])
        return pixbuf


    def create_artists_label(album):
        label = ', '.join(album.get_albumartists())
        if album.get_artists():
            label = locale.gettext("{} feat. {}").format(
                label,
                ", ".join(album.get_artists())
            )
        return label


    def create_length_label(album):
        minutes = album.get_length() // 60
        seconds = album.get_length() - minutes * 60

        return locale.gettext("{}:{} minutes").format(minutes, seconds)


    def create_track_title(track):
        title = track.get_title()
        if track.get_artists():
            title = locale.gettext("{} feat. {}").format(
                title,
                ", ".join(track.get_artists())
            )
        return title


    def generate_id(values):
        if type(values) is not list:
            values = [values]
        m = hashlib.md5()
        for value in values:
            m.update(value.encode('utf-8'))
        return m.hexdigest()




class SortOrder:
    ARTIST = 0
    TITLE = 1
    YEAR = 2
    MODIFIED = 3




class GridItem(GObject.GObject):
    __gtype_name__ = "GridItem"

    tooltip = GObject.Property(type=str, default=None)
    cover = GObject.Property(type=Gdk.Paintable, default=None)


    def __init__(self, album, cover):
        super().__init__()
        self._album = album
        if cover:
            self.cover = Gdk.Texture.new_for_pixbuf(cover)
        self.tooltip = GObject.markup_escape_text("\n".join([
            album.get_title(),
            ', '.join(album.get_dates()),
            Utils.create_artists_label(album),
            Utils.create_length_label(album)
        ]))


    def get_album(self):
        return self._album


    def set_cover(self, cover):
        self.cover = Gdk.Texture.new_for_pixbuf(cover)




class SearchFilter(Gtk.Filter):


    def __init__(self, search_string):
        super().__init__()
        self._search_string = search_string


    def do_match(self, grid_item):
        return grid_item.get_album().filter(self._search_string)
