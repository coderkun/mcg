��    :      �  O   �      �     �                    "     )     2     E     [     a     m     t     z     �     �     �     �     �     �     �     �     �     �     �     �          '     /     >     N     e     l  !   �     �     �     �  
   �     �     �     �          3     O     l     s     x     ~     �     �     �     �     �     �     �     �     �     
  �       �	     �	     �	     �	     �	     �	     �	     	
     
     %
     1
     8
     >
     F
     K
     S
     a
     p
     
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          "     )  !   >  )   `     �  
   �  
   �     �     �     �     �               ;     B     G     M     T  	   c     m     �     �     �     �     �     �     �                !      4               .       :   *   $             &   7             +   3                 2       #   	          -      9       (      8      ,      
               1   %           6   /                             "   )   0   '                              5            <i>none</i> Albums Artists Audio Devices Audio: Bitrate: Clear the playlist Connect or disconnect Cover Cover Panel Error: File: General Host Library Library Panel Loading albums Loading images No service found Open the info dialog Password Player Playlist Port Quit the application Search the library Seconds Seconds played Seconds running Select multiple albums Server Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts Songs Sort Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Library panel Switch to the Playlist panel cancel play queue remove search library sort by artist sort by modification sort by title sort by year sort library descending update library use {} feat. {} {}:{} minutes Project-Id-Version: CoverGrid (mcg)
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-05-22 14:39+0200
Last-Translator: coderkun <olli@suruatoel.xyz>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.4.2
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: mcg
X-Poedit-SearchPath-1: data/ui
 <i>none</i> Albums Artists Audio Devices Audio: Bitrate: Clear the playlist Connect or disconnect Cover Cover Panel Error: File: General Host Library Library Panel Loading albums Loading images No service found Open the info dialog Password Player Playlist Port Quit the application Search the library Seconds Seconds Seconds running Select multiple albums Server Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts (this dialog) Songs Sort order Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Cover panel Switch to the Playlist panel cancel play queue remove search library by Artist by Modification Date by Title by Year sort descending update the library use {} feat. {} {}:{} minutes 